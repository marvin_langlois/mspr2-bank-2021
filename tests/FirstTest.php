<?php

namespace App\Tests;

use App\Entity\Message;
use PHPUnit\Framework\TestCase;

class FirstTest extends TestCase
{
    public function testMessageEntity()
    {
        $message = new Message();
        $message->setContent("Bonjour, bienvenue sur votre ERP preud'Homme !");

        $this->assertTrue($message->getContent() === "Bonjour, bienvenue sur votre ERP preud'Homme !");
    }
}
