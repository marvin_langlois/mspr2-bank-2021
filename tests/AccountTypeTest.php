<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AccountTypeTest extends WebTestCase
{

    public function testBuildDisplayNewAccount()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/account/type/new');

        //$this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Ajouter un nouveau type de compte');
    }

/**
    public function testShouldAddNewAccountType()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/account/type/new');

        $btnCrawlerNode = $crawler->selectButton('Save');

        $uuid = uniqid();

        $form = $btnCrawlerNode->form([
            'account_type[name]' => 'Test account' . $uuid,
            'account_type[details]' => 'Test account details' . $uuid,
            'account_type[rate]' => 'Test account rate' . $uuid
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
    }
**/


    public function testBuildDisplayHome()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/');

        //$this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'PreudHomme');
    }
}
