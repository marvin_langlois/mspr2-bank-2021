<?php

namespace App\Controller;

use App\Entity\AccountType;
use App\Form\AccountTypeType;
use App\Repository\AccountTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account/type")
 */
class AccountTypeController extends AbstractController
{
    /**
     * @Route("/", name="account_type_index", methods={"GET"})
     * @param AccountTypeRepository $accountTypeRepository
     * @return Response
     */
    public function index(AccountTypeRepository $accountTypeRepository): Response
    {
        return $this->render('account_type/index.html.twig', [
            'account_types' => $accountTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="account_type_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $accountType = new AccountType();
        $form = $this->createForm(AccountTypeType::class, $accountType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($accountType);
            $entityManager->flush();

            return $this->redirectToRoute('account_type_index');
        }

        return $this->render('account_type/new.html.twig', [
            'account_type' => $accountType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="account_type_show", methods={"GET"})
     * @param AccountType $accountType
     * @return Response
     */
    public function show(AccountType $accountType): Response
    {
        return $this->render('account_type/show.html.twig', [
            'account_type' => $accountType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="account_type_edit", methods={"GET","POST"})
     * @param Request $request
     * @param AccountType $accountType
     * @return Response
     */
    public function edit(Request $request, AccountType $accountType): Response
    {
        $form = $this->createForm(AccountTypeType::class, $accountType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('account_type_index');
        }

        return $this->render('account_type/edit.html.twig', [
            'account_type' => $accountType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="account_type_delete", methods={"DELETE"})
     * @param Request $request
     * @param AccountType $accountType
     * @return Response
     */
    public function delete(Request $request, AccountType $accountType): Response
    {
        if ($this->isCsrfTokenValid('delete' . $accountType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($accountType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('account_type_index');
    }
}
